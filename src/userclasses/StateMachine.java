/**
 * Your application code goes here
 */
package userclasses;

import com.codename1.facebook.FaceBookAccess;
import com.codename1.io.Oauth2;
import com.codename1.ui.plaf.UIManager;
import com.naresh.se.ordnafika.FIKAStorage;
import com.naresh.se.ordnafika.OrdnaUtility;
import generated.StateMachineBase;
import com.codename1.ui.*; 
import com.codename1.ui.events.*;
import com.codename1.ui.util.Resources;

import java.util.Hashtable;

/**
 * Created by Naresh Mehta on 2015-01-30.
 */
public class StateMachine extends StateMachineBase {
    private static String FBAPPID_CLIENTID = "499377413536483";
    private static String FBAPPSECRET_CLIENTSECRET = "1142953a64b86f3d6579be7f4bf0315d";
    private static String FBREDIRECTURL = "http://www.naresh.se/";

    public StateMachine(String resFile) {
        super(resFile);
        // do not modify, write code in initVars and initialize class members there,
        // the constructor might be invoked too late due to race conditions that might occur
    }
    
    /**
     * this method should be used to initialize variables instead of
     * the constructor/class scope to avoid race conditions
     */
    protected void initVars(Resources res) {
    }

    @Override
    protected void onMain_BtnFacebookRegisterAction(Component c, ActionEvent event) {
        FaceBookAccess.setClientId(FBAPPID_CLIENTID);
        FaceBookAccess.setClientSecret(FBAPPSECRET_CLIENTSECRET);
        FaceBookAccess.setRedirectURI(FBREDIRECTURL);
        if(FIKAStorage.isRegisteredUser()) {
            OrdnaUtility.loginUserWithToken();
        } else {
            connectWithFB();
        }
    }

    @Override
    protected void onMain_BtnDeleteAccountAction(Component c, ActionEvent event) {
        FIKAStorage.deleteAccount();
    }

    /* Connect to the OrdnaFIKA Application in Facebook! */
    private void connectWithFB() {
        FaceBookAccess.getInstance().showAuthentication(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getSource() instanceof String) {
                    String token = (String) evt.getSource();
                    FIKAStorage.setRegisteredFBUser(token);
                    System.out.println("recived a token " + token);
                    OrdnaUtility.loginUserWithToken();
                    Display.getInstance().callSerially(new Runnable() {
                        @Override
                        public void run() {
                            findTabs().setSelectedIndex(1);
                        }
                    });
                } else {
                    Exception err = (Exception) evt.getSource();
                    err.printStackTrace();
                    Dialog.show("Error", "An error occurred while logging in: " + err, "OK", null);
                }
            }
        });
    }

    @Override
    protected void postMain(Form f) {
        if(!FIKAStorage.isRegisteredUser()) {
            findTabs().setSelectedIndex(0);
        } else {
            OrdnaUtility.loginUserWithToken();
            findTabs().setSelectedIndex(1);
        }
    }

}
