package com.naresh.se.ordnafika;

import com.codename1.facebook.FaceBookAccess;
import com.codename1.facebook.User;
import com.codename1.io.NetworkEvent;
import com.codename1.io.Oauth2;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;

import java.io.IOException;

/**
 * Created by Naresh Mehta on 2015-01-31.
 */
public class OrdnaUtility {

    public static boolean loginUserWithToken() {
        FIKAStorage.TOKEN_TYPE_ENUM token_type;
        if(!FIKAStorage.isRegisteredUser()) {
            System.out.println("Error! We should not be here! User is not registered!");
            return false;
        }
        token_type = FIKAStorage.TOKEN_TYPE_ENUM.values()[FIKAStorage.getTokenType()];
        switch(token_type) {
            case FACEBOOK:
                String strFBToken = FIKAStorage.getFBToken();
                FaceBookAccess.setToken(strFBToken);
                FaceBookAccess.getInstance().addResponseCodeListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        NetworkEvent ne = (NetworkEvent) evt;
                        int responseCode = ne.getResponseCode();
                        if(400 == responseCode) {
                            System.out.println("Token Expired! Lets login again!");
                        }
                    }
                });
                break;
            case TWITTER:
                break;
            case EMAIL:
                break;
            default: // Also UNKNOWN
                return false;
        }
        final User me = new User();
        try {
            FaceBookAccess.getInstance().getUser("me", me, new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    try {
                        System.out.println("Name: " + me.getName());
                        System.out.println("Birthday: " + me.getBirthday());
                        System.out.println("Status: " + me.getRelationship_status());
                        System.out.println("Email: " + me.getEmail());
                        FaceBookAccess.getInstance().getPhotoThumbnail(me.getId(), new ActionListener() {
                            public void actionPerformed(ActionEvent evt) {
                                if (evt != null) {
                                    System.out.println("Got the photo successfully!");
                                }
                            }
                        }, true);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        ParseRESTServerStore restStore = new ParseRESTServerStore();
        restStore.initializeRESTStore();
        return true;
    }
}
