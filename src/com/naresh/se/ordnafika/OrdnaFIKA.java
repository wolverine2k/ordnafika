package com.naresh.se.ordnafika;

import com.codename1.ui.*;
import com.codename1.ui.util.UIBuilder;
import generated.StateMachineBase;
import userclasses.StateMachine;

/**
 * Created by Naresh Mehta on 2015-01-30.
 */
public class OrdnaFIKA {

    private Form current;

    public void init(Object context) {
    }

    public void start() {
        if(current != null){
            current.show();
            return;
        }
        new StateMachine("/theme");
    }

    public void stop() {
        current = Display.getInstance().getCurrent();
    }

    public void destroy() {
    }
}
