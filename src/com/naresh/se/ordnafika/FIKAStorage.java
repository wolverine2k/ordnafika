package com.naresh.se.ordnafika;

import com.codename1.db.Database;
import com.codename1.io.Preferences;

/**
 * Created by Naresh Mehta on 2015-01-31.
 */
public class FIKAStorage {
    private static String STR_FB_TOKEN = "TOKEN";
    private static String STR_IS_REGISTERED_USER = "IS_REGISTERED_USER";
    private static String STR_TOKEN_TYPE = "TOKEN_TYPE";

    public static boolean isRegisteredUser() {
        boolean result = false;
        result = Preferences.get(STR_IS_REGISTERED_USER, result);
        return result;
    }

    public static void setRegisteredFBUser(String token) {
        Preferences.set(STR_FB_TOKEN, token);
        Preferences.set(STR_TOKEN_TYPE, TOKEN_TYPE_ENUM.FACEBOOK.ordinal());
        Preferences.set(STR_IS_REGISTERED_USER, true);
    }

    public static void deleteAccount() {
        Preferences.delete(STR_IS_REGISTERED_USER);
        Preferences.delete(STR_FB_TOKEN);
    }

    public static int getTokenType() {
        int result = 0;
        result = Preferences.get(STR_TOKEN_TYPE, result);
        return result;
    }

    public static String getFBToken() {
        String token = "";
        token = Preferences.get(STR_FB_TOKEN, token);
        return token;
    }

    public enum TOKEN_TYPE_ENUM {
        FACEBOOK,
        TWITTER,
        EMAIL,
        UNKNOWN,
    }
}
